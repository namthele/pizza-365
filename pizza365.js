$(document).ready(function () {
  console.log("Welcome pizza365");
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //Khai báo đối tượng chứa thông tin menu pizza được chọn
  const gBASE_URL = "http://42.115.221.44:8080";

  var gSelectedMenuStructure = {
    kichCo: "",
    duongKinh: "",
    suon: 0,
    salad: "",
    soLuongNuoc: 0,
    thanhTien: 0,
  };
  var gUserOder = {
    kichCo: "",
    duongKinh: "",
    suon: 0,
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    thanhTien: 0,
    giamGia: 0,
    idLoaiNuocUong: "",
    soLuongNuoc: 2,
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
  };
  // Định nghĩa đối tượng chứa thông tin Pizza Type
  var gSelectPizzaType = "";
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  // Vùng chạy khi load trang
  onPageLoading();
  //Khi bấm nút chọn menu S

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  $("#btn-size-s").on("click", function () {
    onBtnSizeSChosen();
  });
  //Khi bấm nút chọn menu M
  $("#btn-size-m").on("click", function () {
    onBtnSizeMChosen();
  });
  //Khi bấm nút chọn menu L
  $("#btn-size-l").on("click", function () {
    onBtnSizeLChosen();
  });
  $("#btn-hawaii").on("click", function () {
    onBtnHawaiiClick();
  });
  $("#btn-hai-san").on("click", function () {
    onBtnHaiSanClick();
  });
  $("#btn-thit-hun-khoi").on("click", function () {
    onBtnThitHunKhoi();
  });
  $("#btn-sendorder").on("click", function () {
    onBtnSendOrderClick();
  });
  $("#btn-create").on("click", function () {
    onBtnTaoDonClick();
  });
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Vùng viết hàm cho toàn chương trình
  function onPageLoading() {
    "use strict";
    callAPIGetDrinkList();
  }
  // Hàm xử lý khi bấm vào nút tạo đơn
  function onBtnTaoDonClick() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/devcamp-pizza365/orders",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(gUserOder),
      success: function (res) {
        console.log(res);
        $("#order-modal").modal("hide");
        $("#inp-orderid").val(res.orderId);
        $("#create-confirm-modal").modal("show");
      },
    });
  }
  // Hàm xử lý khi bấm vào nút gửi đơn hàng
  function onBtnSendOrderClick() {
    "use strict";
    resetErrors();
    console.log("Bạn vừa bấm vào nút Gửi đơn hàng !");
    // B1: lấy thông tin từ form
    getInfoOrder(gUserOder);
    callAPIGetVoucherByVoucherId(gUserOder.idVourcher);
    // B2: kiểm tra dữ liệu
    var vIsValidate = validateUserInfo(gUserOder);
    if (vIsValidate) {
      loadDataToModal(gUserOder);

      $("#order-modal").modal("show");
    } else {
      $("#lbl-thongbao").css("display", "block");
      console.log("input false...");
    }
  }
  // Hàm xử lý load API lấy phần trăm giảm giá
  function callAPIGetVoucherByVoucherId(paramVoucherId) {
    "use strict";
    var vPhanTramGiamGia = 0;
    $.ajax({
      url: gBASE_URL + "/devcamp-voucher-api/voucher_detail/" + paramVoucherId,
      type: "GET",
      async: false,
      dataType: "json",
      success: function (res) {
        vPhanTramGiamGia = parseInt(res.phanTramGiamGia);
      },
      error: function (err) {
        console.log(err.message);
      },
    });
    return vPhanTramGiamGia;
  }
  // Hàm xử lý load thông tin vào modal thông tin đơn hàng
  function loadDataToModal(paramUserOrder) {
    "use strict";
    var vPhanTramGiamGia = callAPIGetVoucherByVoucherId(
      paramUserOrder.idVourcher
    );
    console.log("ss: " + vPhanTramGiamGia);
    var vPhaiThanhToan =
      paramUserOrder.thanhTien -
      (paramUserOrder.thanhTien * vPhanTramGiamGia) / 100;
    var vGia =
      paramUserOrder.thanhTien.toString().slice(0, 3) +
      "." +
      paramUserOrder.thanhTien.toString().slice(3, 6);
    var vThanhToan =
      vPhaiThanhToan.toString().slice(0, 3) +
      "." +
      vPhaiThanhToan.toString().slice(3, 6);
    console.log(vGia);
    $("#inp-email-m").val(paramUserOrder.email);
    $("#inp-hoten").val(paramUserOrder.hoTen);
    $("#inp-sodienthoai").val(paramUserOrder.soDienThoai);
    $("#inp-diachi").val(paramUserOrder.diaChi);
    $("#inp-magiamgia").val(paramUserOrder.idVourcher);
    $("#inp-loinhan").val(paramUserOrder.loiNhan);
    $(
      "#inp-info"
    ).html(`    Xác nhận: ${paramUserOrder.hoTen}, ${paramUserOrder.soDienThoai}, ${paramUserOrder.diaChi}
    Menu ${paramUserOrder.kichCo}, sườn nướng ${paramUserOrder.suon}, nước ${paramUserOrder.soLuongNuoc}, ...
    Loại pizza: ${paramUserOrder.loaiPizza}, Giá: ${vGia} vnd, Mã giảm giá: ${paramUserOrder.idVourcher}
    Phải thanh toán: ${vThanhToan} (giảm giá ${vPhanTramGiamGia} %)`);
  }
  // Hàm xử lý reset thông báo lỗi nhập liệu
  function resetErrors() {
    "use strict";
    $("#lbl-fullname").css("display", "none");
    $("#lbl-email").css("display", "none");
    $("#lbl-dien-thoai").css("display", "none");
    $("#lbl-diachi").css("display", "none");
    $("#lbl-thongbao").css("display", "none");
  }
  // Hàm xử lý kiểm tra tính đúng đắn của dữ liệu nhập vào
  function validateUserInfo(paramUserOrder) {
    "use strict";
    var filter =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (paramUserOrder.kichCo === "") {
      var bOffset = $("#plans").offset();
      $("html, body").animate(
        {
          scrollTop: bOffset.top,
          scrollLeft: bOffset.left,
        },
        1000
      );
      alert("Bạn chưa chọn cỡ Pizza !!");
      return false;
    }
    if (paramUserOrder.loaiPizza === "") {
      var bOffset = $("#about").offset();
      $("html, body").animate(
        {
          scrollTop: bOffset.top,
          scrollLeft: bOffset.left,
        },
        1000
      );
      alert("Bạn chưa chọn loại Pizza !!");
      return false;
    }
    if (paramUserOrder.idLoaiNuocUong === "") {
      var bOffset = $("#drink").offset();
      $("html, body").animate(
        {
          scrollTop: bOffset.top,
          scrollLeft: bOffset.left,
        },
        1000
      );
      alert("Bạn chưa chọn nước uống !");
      return false;
    }
    if (paramUserOrder.hoTen === "") {
      $("#lbl-fullname").css("display", "block");
      $("#inp-fullname").focus();
      return false;
    }
    if (paramUserOrder.email === "" || !filter.test(paramUserOrder.email)) {
      $("#lbl-email").css("display", "block");
      $("#inp-email").focus();
      return false;
    }
    if (paramUserOrder.soDienThoai === "") {
      $("#lbl-dien-thoai").css("display", "block");
      $("#inp-dien-thoai").focus();
      return false;
    }
    if (paramUserOrder.diaChi === "") {
      $("#lbl-diachi").css("display", "block");
      $("#inp-dia-chi").focus();
      return false;
    } else {
      return true;
    }
  }

  // Hàm xử lý load dữ liệu đổ vào select drink
  function loadDataToSelectDrink(paramDrinkArr) {
    "use strict";
    var vSelectDrink = $("#select-drink");
    $("<option/>", {
      value: "",
      text: "Tất cả loại nước uống ",
    }).appendTo(vSelectDrink);
    paramDrinkArr.map((data) => {
      $("<option/>", {
        vaule: data.maNuocUong,
        text: data.tenNuocUong,
      }).appendTo(vSelectDrink);
    });
  }
  // Hàm xử lý khi bấm vào chọn loại pizza thịt hun khói
  function onBtnThitHunKhoi() {
    "use strict";
    gSelectPizzaType = "Bacon";
    changeColorButtonPizzaType(gSelectPizzaType);
    console.log("Bạn vừa chọn loại Pizza là: " + gSelectPizzaType);
  }
  // Hàm xử lý khi bấm vào chọn loại Pizza Hải sản
  function onBtnHaiSanClick() {
    "use strict";
    gSelectPizzaType = "Seafood";
    changeColorButtonPizzaType(gSelectPizzaType);
    console.log("Bạn vừa chọn loại Pizza là: " + gSelectPizzaType);
  }
  // Hàm xử lý khi bấm vào chọn loại Pizza Hawaii
  function onBtnHawaiiClick() {
    "use strict";
    gSelectPizzaType = "Hawaii";
    changeColorButtonPizzaType(gSelectPizzaType);
    console.log("Bạn vừa chọn loại Pizza là: " + gSelectPizzaType);
  }
  //Hàm xử lý khi bấm vào nút Chọn Size S----------------
  function onBtnSizeSChosen() {
    "use strict";
    console.log("Size S Chosen");
    // Định nghĩa đối tượng Pizza size S
    var vPizzaSizeS = {
      kichCo: "S",
      duongKinh: "20cm",
      suon: 2,
      salad: "200g",
      soLuongNuoc: 2,
      thanhTien: 150000,
    };
    // Bước 1: Lấy thông tin
    gSelectedMenuStructure = vPizzaSizeS;
    // Bước 2: validate. bỏ qua
    // Bước 3: Xử lý thông tin
    changeColorButtonMenuClick(gSelectedMenuStructure.kichCo);
    // Bước 4: Xuất thông tin
    console.log(gSelectedMenuStructure);
  }
  //Hàm xử lý khi bấm vào nút Chọn Size M
  function onBtnSizeMChosen() {
    "use strict";
    // Định nghĩa đối tượng Pizza size M
    var vPizzaSizeM = {
      kichCo: "M",
      duongKinh: "25cm",
      suon: 4,
      salad: "300g",
      soLuongNuoc: 3,
      thanhTien: 200000,
    };
    console.log("Size M Chosen");
    // Bước 1: Lấy thông tin
    gSelectedMenuStructure = vPizzaSizeM;
    // Bước 2: validate. bỏ qua
    // Bước 3: Xử lý thông tin
    changeColorButtonMenuClick(gSelectedMenuStructure.kichCo);
    // Bước 4: Xuất thông tin
    console.log(gSelectedMenuStructure);
  }
  //Hàm xử lý khi bấm vào nút Chọn Size L
  function onBtnSizeLChosen() {
    "use strict";
    // Định nghĩa đối tượng Pizza size L
    var vPizzaSizeL = {
      kichCo: "L",
      duongKinh: "30cm",
      suon: 8,
      salad: "500g",
      soLuongNuoc: 4,
      thanhTien: 250000,
    };
    console.log("Size L Chosen");
    // Bước 1: Lấy thông tin
    gSelectedMenuStructure = vPizzaSizeL;
    // Bước 2: validate. bỏ qua
    // Bước 3: Xử lý thông tin
    changeColorButtonMenuClick(gSelectedMenuStructure.kichCo);
    // Bước 4: Xuất thông tin
    console.log(gSelectedMenuStructure);
  }
  //Hàm xử lý đổi màu khi click chọn menu pizza
  function changeColorButtonMenuClick(paramMenuSize) {
    "use strict";
    if (paramMenuSize == "S") {
      $("#btn-size-s").removeClass("btn-warning");
      $("#btn-size-s").addClass("btn-success");
      $("#btn-size-m, #btn-size-l").addClass("btn-warning");
    }
    if (paramMenuSize == "M") {
      $("#btn-size-m").removeClass("btn-warning");
      $("#btn-size-m").addClass("btn-success");
      $("#btn-size-s, #btn-size-l").addClass("btn-warning");
    }
    if (paramMenuSize == "L") {
      $("#btn-size-l").removeClass("btn-warning");
      $("#btn-size-l").addClass("btn-success");
      $("#btn-size-m, #btn-size-s").addClass("btn-warning");
    }
  }
  // Hàm xử lý lấy dữ liệu thông tin đơn hàng
  function getInfoOrder(paramOrder) {
    "use strict";
    paramOrder.kichCo = gSelectedMenuStructure.kichCo;
    paramOrder.duongKinh = gSelectedMenuStructure.duongKinh;
    paramOrder.suon = gSelectedMenuStructure.suon;
    paramOrder.salad = gSelectedMenuStructure.salad;
    paramOrder.loaiPizza = gSelectPizzaType;
    paramOrder.idVourcher = $("#inp-voucherid").val();
    paramOrder.thanhTien = gSelectedMenuStructure.thanhTien;
    paramOrder.idLoaiNuocUong = $("#select-drink").val();
    paramOrder.soLuongNuoc = gSelectedMenuStructure.soLuongNuoc;
    paramOrder.hoTen = $("#inp-fullname").val().trim();
    paramOrder.email = $("#inp-email").val().trim();
    paramOrder.soDienThoai = $("#inp-dien-thoai").val().trim();
    paramOrder.diaChi = $("#inp-dia-chi").val().trim();
    paramOrder.loiNhan = $("#inp-message").val().trim();
  }
  // Hàm xử lý đổi màu nút khi bấm nút chọn loại pizza
  function changeColorButtonPizzaType(paramPizzaType) {
    "use strict";
    if (paramPizzaType == "Hawaii") {
      $("#btn-hawaii").removeClass("btn-warning");
      $("#btn-hawaii").addClass("btn-success");
      $("#btn-hai-san, #btn-thit-hun-khoi").addClass("btn-warning");
    }
    if (paramPizzaType == "Seafood") {
      $("#btn-hai-san").removeClass("btn-warning");
      $("#btn-hai-san").addClass("btn-success");
      $("#btn-hawaii, #btn-thit-hun-khoi").addClass("btn-warning");
    }
    if (paramPizzaType == "Bacon") {
      $("#btn-thit-hun-khoi").removeClass("btn-warning");
      $("#btn-thit-hun-khoi").addClass("btn-success");
      $("#btn-hawaii, #btn-hai-san").addClass("btn-warning");
    }
  }
  // Hàm gọi API lấy danh sách nước uống
  function callAPIGetDrinkList() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/devcamp-pizza365/drinks",
      type: "GET",
      dataType: "json",
      success: function (res) {
        loadDataToSelectDrink(res);
      },
    });
  }
  //-----------------------end---------------
});
